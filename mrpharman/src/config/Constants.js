import { Dimensions } from 'react-native';

// export const base_url = "http://api.medicoone.com/";
// export const api_url = "http://api.medicoone.com/api/";
// export const settings = "app_setting";
// export const img_url = "http://api.medicoone.com/uploads/";



export const base_url = "http://mrpharman.demoproducts.in/";
export const api_url = "http://mrpharman.demoproducts.in/api/";
export const settings = "app_setting";
export const img_url = "http://mrpharman.demoproducts.in/public/uploads/";



export const category = "category";
export const sub_category = "sub_category";
export const faq = "faq";
export const privacy = "privacy_policy";
export const product = "product";
export const search_products = "search_products";
export const register = "customer";
export const login = "customer/login";
export const address = "address";
export const address_list = "address/all";
export const address_delete = "address/delete";
export const my_orders = "get_orders";
export const promo_code = "promo";
export const profile = "customer";
export const profile_picture = "customer/profile_picture";
export const forgot = "customer/forgot_password";
export const reset = "customer/reset_password";
export const place_order = "order";
export const image_upload = "image_upload";
export const prescription = "prescription";
export const prescription_list = "prescription_list";
export const order_generation = "order_generation";
export const reject_order = "reject_order";
export const get_payment_list = "payment_modes";

export const app_name = "MedicoOne";
export const no_data = "Sorry no data found...";
//Size
export const screenHeight = Math.round(Dimensions.get('window').height);
export const height_40 = Math.round(40 / 100 * screenHeight);
export const height_50 = Math.round(50 / 100 * screenHeight);
export const height_60 = Math.round(60 / 100 * screenHeight);
export const height_35 = Math.round(35 / 100 * screenHeight);
export const height_20 = Math.round(20 / 100 * screenHeight);
export const height_30 = Math.round(30 / 100 * screenHeight);

//Path
export const logo = require('.././assets/img/logo_with_name.png');
export const forgot_password = require('.././assets/img/forgot_password.png');
export const reset_password = require('.././assets/img/reset_password.png');
export const loading = require('.././assets/img/loading.png');
export const pin = require('.././assets/img/location_pin.png');
export const login_image = require('.././assets/img/logo_with_name.png');
export const tablet = require('.././assets/img/tablet.png');
export const list = require('.././assets/img/list.png');
export const upload = img_url + "images/upload.png";

//Map
export const GOOGLE_KEY = "AIzaSyAZRJ0CnwxIsyY7v1KjS3nWtAE3N8mYs8s";
export const LATITUDE_DELTA = 0.0150;
export const LONGITUDE_DELTA = 0.0152;

//More Menu
export const menus = [
  {
    menu_name: 'Profile',
    icon: 'user',
    route: 'Profile'
  },
  {
    menu_name: 'Prescriptions',
    icon: 'document',
    route: 'Prescription'
  },
  {
    menu_name: 'Manage Addresses',
    icon: 'map-pin',
    route: 'AddressList'
  },
  {
    menu_name: 'Faq',
    icon: 'help-circle',
    route: 'Faq'
  },
  {
    menu_name: 'Privacy Policy',
    icon: 'alert-circle',
    route: 'PrivacyPolicy'
  },
  {
    menu_name: 'Contact Us',
    icon: 'phone',
    route: 'ContactUs'
  },
  {
    menu_name: 'Logout',
    icon: 'log-out',
    route: 'Logout'
  },
]

