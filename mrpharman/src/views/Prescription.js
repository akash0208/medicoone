import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Title, Button, Col, Footer, Row } from 'native-base';
import { api_url, list, prescription_list } from '../config/Constants';
import Icon from 'react-native-vector-icons/Feather'
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import Moment from 'moment';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/PrescriptionActions';
import ProgressCircle from 'react-native-progress-circle';
class Prescription extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.Prescription();
    });
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  add_prescription = () => {
    this.props.navigation.navigate('AddPrescription');
  }

  view_prescription = (data) => {
    this.props.navigation.navigate('ViewPrescription', { data: data });
  }

  Prescription = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'post',
      url: api_url + prescription_list,
      data: { customer_id: global.id }
    })
      .then(async response => {
        await this.props.serviceActionSuccess(response.data)
      })
      .catch(error => {
        this.props.serviceActionError(error);
      });
  }


  render() {

    const { isLoding, error, data, message, status } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Prescriptions</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <List>
            {data.map((row, index) => (
              <ListItem onPress={() => this.view_prescription(row)}>
                <Col style={{ width: '30%' }}>
                  <ProgressCircle
                    percent={row.status * 13}
                    radius={30}
                    borderWidth={3}
                    color={colors.theme_fg}
                    shadowColor="#e6e6e6"
                    bgColor="#FFFFFF"
                  >
                    <View style={{ height: 30, width: 30 }} >
                      <Image
                        style={{ flex: 1, width: undefined, height: undefined }}
                        source={list}
                      />
                    </View>
                  </ProgressCircle>
                </Col>
                <Col style={{ width: '50%' }} >
                  <Text style={styles.order_id}>Prescription order Id : #{row.id}</Text>
                  <View style={{ margin: 1 }} />
                  <Text style={{ fontSize: 10 }} >{Moment(row.created_at).format('DD MMM-YYYY hh:mm')}</Text>
                  {row.status == 9 ? <Text style={{ color: colors.red }} >{row.status_name}</Text> : <Text style={{ color: colors.theme_bg }} >{row.status_name}</Text>}

                </Col>
              </ListItem>
            ))}
          </List>
        </Content>
        <Footer style={styles.footer} >
          <TouchableOpacity activeOpacity={1} onPress={() => this.add_prescription()} style={styles.footer_container}>
            <Row>
              <Col style={styles.view_cart_container} >
                <Text style={styles.view_cart} >ADD PRESCRIPTION</Text>
              </Col>
            </Row>
          </TouchableOpacity>
        </Footer>
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.prescription.isLoding,
    error: state.prescription.error,
    data: state.prescription.data,
    message: state.prescription.message,
    status: state.prescription.status,
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(Prescription);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_container: {
    width: '100%',
    backgroundColor: colors.theme_fg
  },
  view_cart_container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  view_cart: {
    color: colors.theme_bg,
    fontWeight: 'bold'
  }
});
