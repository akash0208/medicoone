import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Row, Footer, Col, Content, Button } from 'native-base';
import { img_url, api_url, product, height_30, no_data } from '../config/Constants';
import Icon from 'react-native-vector-icons/Feather'
import { Loader } from '../components/GeneralComponents';
import * as colors from '../assets/css/Colors';
import axios from 'axios';
import { Badge } from 'react-native-elements';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess, addToCart } from '../actions/ProductActions';
import {  subTotal } from '../actions/CartActions';

class Product extends Component<Props> {

  constructor(props) {
      super(props)
      this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
      this.state = {
        data:this.props.route.params.data
      }
  }

  async componentDidMount(){
  this._unsubscribe=this.props.navigation.addListener('focus',()=>{
    this.Product();
  });
}
componentWillUnmount(){
  this._unsubscribe();
}

  Product = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'post', 
      url: api_url + product,
      data:{ sub_category_id: this.state.data.id }
    })
    .then(async response => {
      await this.props.serviceActionSuccess(response.data)
    })
    .catch(error => {
        this.props.serviceActionError(error);
    });
  }

  handleBackButtonClick= () => {
      this.props.navigation.goBack(null);
  }

  cart = () => {
    this.props.navigation.navigate('Cart');
  }

  move_product_details = (item) =>{
    this.props.navigation.navigate('ProductDetails',{ data : item });
  }

  render() {

    const { isLoding, error, data, message, status, cart_items, cart_count } = this.props
    
    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >{this.state.data.sub_category_name}</Title>
          </Body>
          <Right style={{ marginRight:10 }}>
            <TouchableOpacity activeOpacity={1} onPress={this.cart} >
            <Icon style={styles.icon} name='cart' />
            { this.props.cart_count > 0 && <Badge
              status="error"
              containerStyle={{ position: 'absolute', top: -6, right: -6 }}
              value={this.props.cart_count}
            />}
            </TouchableOpacity>
          </Right>
        </Header>
        { data != 0 && data != undefined && 
        <Content>
          <View>
            <Image source={{ uri : img_url + this.state.data.image }} style={{ width:'100%', height:180 }}/>
          </View>
          <View style={{ margin:10 }} />
          {data.map((row, index) => (
            <TouchableOpacity activeOpacity={1} onPress={() => this.move_product_details(row)} >
              <Row style={{ borderRadius:10, marginTop:10, width:'94%', borderWidth:1, padding:10, marginLeft:'3%', borderColor:colors.grey }} >
                <Col style={{ width:100 }}>
                  <View style={styles.image_container} >
                    <Image
                      style= {{flex:1 , width: undefined, height: undefined, borderRadius:10 }}
                      source={{uri : img_url + row.image }}
                    />
                  </View>
                </Col>
                <Col style={{ justifyContent:'center'}}>
                  <Text style={styles.product_name} >{row.product_name}</Text>
                  <View style={{ margin:2 }} />
                  
                </Col>
              </Row>
            </TouchableOpacity>
          ))}
        </Content>
        }
        {data == 0 && <Body style={{ marginTop:height_30 }} >
            <Text>{no_data}</Text>
          </Body>}
        {cart_count ?
          <Footer style={styles.footer} >
            <TouchableOpacity activeOpacity={1} onPress={() => this.cart()} style={styles.footer_container}>
              <Row>
                <Col style={styles.view_cart_container} >
                  <Text style={styles.view_cart} >VIEW CART</Text>
                </Col>
              </Row>
            </TouchableOpacity>
          </Footer> : null }
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state){
  return{
    isLoding : state.product.isLoding,
    error : state.product.error,
    data : state.product.data,
    message : state.product.message,
    status : state.product.status,
    cart_items : state.product.cart_items,
    cart_count : state.product.cart_count,
    sub_total : state.cart.sub_total,
    cart_count : state.product.cart_count
  };
}

const mapDispatchToProps = (dispatch) => ({
    serviceActionPending: () => dispatch(serviceActionPending()),
    serviceActionError: (error) => dispatch(serviceActionError(error)),
    serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data)),
});


export default connect(mapStateToProps,mapDispatchToProps)(Product);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.theme_bg_two,
  },
  header:{
    backgroundColor:colors.theme_bg
  },
  icon:{
    color:colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title:{
    alignSelf:'center', 
    color:colors.theme_fg,
    alignSelf:'center', 
    fontSize:16, 
    fontWeight:'bold'
  },
  image_container:{
    height:75, 
    width:75
  },
  product_name:{
    fontSize:15, 
    fontWeight:'bold', 
    color:colors.theme_fg_two
  },
  price:{
    fontSize:18, 
    fontWeight:'bold', 
    color:colors.theme_bg,
  },
  piece:{
    fontSize:12, 
    color:colors.theme_fg
  },
  footer:{
    backgroundColor:'transparent'
  },
  footer_container:{
    width:'100%', 
    backgroundColor:colors.theme_fg
  },
  view_cart_container:{
    alignItems:'center',
    justifyContent:'center'
  },
  view_cart:{
    color:colors.theme_bg, 
    fontWeight:'bold'
  }
});

