import React, { Component } from 'react';
import { StyleSheet, Text, Image, View } from 'react-native';
import { Content, Container, Header, Body, Title, Left, Row, Right, Button as Btn } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import * as colors from '../assets/css/Colors';
import { logo } from '../config/Constants';
import { StatusBar } from '../components/GeneralComponents';
import { Button, Divider } from 'react-native-elements';
import { app_name } from '../config/Constants';

class ContactUs extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  show_alert(message) {
    this.dropDownAlertRef.alertWithType('error', 'Error', message);
  }

  render() {
    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={styles.flex_1} >
            <Btn onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Btn>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Contact Us</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Row style={{ backgroundColor: colors.theme_bg }}>
            <Body style={{ backgroundColor: colors.theme_bg }} >
              <View style={styles.logo}>
                <Image
                  style={{ flex: 1, width: undefined, height: undefined, borderRadius: 50 }}
                  source={logo}
                />
              </View>
            </Body>
          </Row>
          <Divider style={styles.default_divider} />
          <View style={{ margin: 10 }} />
          <Row>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: colors.theme_fg_two }}>Contact details</Text>
          </Row>
          <View style={{ margin: 10 }} />
          <Row>
            <Icon style={{ fontSize: 18, marginRight: 10 }} name='phone' />
            <Text style={{ fontSize: 14, color: colors.theme_fg_four }}>
              {/* {global.admin_phone} */}
              +91-9999-140-200
              </Text>
          </Row>
          <View style={{ margin: 5 }} />
          <Row>
            <Icon style={{ fontSize: 18, marginRight: 10 }} name='mail' /><Text style={{ fontSize: 14, color: colors.theme_fg_four }}>
              {/* {global.admin_email} */}
              sales@medicoone.com
            </Text>
          </Row>
          <View style={{ margin: 5 }} />
          <Row>
            <Icon style={{ fontSize: 18, marginRight: 10 }} name='map-pin' /><Text style={{ fontSize: 14, color: colors.theme_fg_four }}>
              {/* {global.admin_address} */}
              CM-18, 1st Floor, Sector-122, Noida, UP, 201301
            </Text>
          </Row>
        </Content>
      </Container>
    );
  }
}

export default ContactUs;

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  flex_1: {
    flex: 1
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold'
  },
  margin_20: {
    margin: 20
  },
  margin_10: {
    margin: 10
  },
  margin_50: {
    margin: 50
  },
  padding_20: {
    padding: 20
  },
  logo: {
    height: 200,
    width: 200,
    backgroundColor: colors.theme_bg
  },
  default_divider: {
    marginTop: 15,
    marginBottom: 20
  },

});
