import React, { Component } from 'react';
import { StyleSheet, Text, Image, TouchableOpacity, Keyboard, View, TextInput } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Button, Form, Textarea, Card, CardItem, Col, Footer, Row } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import { api_url, upload, image_upload, prescription } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import { launchImageLibrary } from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/CreatePrescriptionActions';

const options = {
  mediaType: 'photo',
  includeBase64: true,
};

class AddPrescription extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      image_one: { uri: upload },
      image_two: { uri: upload },
      image_three: { uri: upload },
      prescription_one: '',
      prescription_two: '',
      prescription_three: '',
      customer_notes: '',
      data: '',
      images: [],
      validation: true,
      isLoding: false,
      doctor_name: ''
    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  select_address = () => {
    this.props.navigation.navigate("AddressList", { from: "prescription" });
  }

  select_photo(type) {
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        console.log(response.data, response)
        if (type == 1) {
          this.setState({
            data: response.base64,
            image_one: { uri: response.uri }
          });
        } else if (type == 2) {
          this.setState({
            data: response.base64,
            image_two: { uri: response.uri }
          });
        } else if (type == 3) {
          this.setState({
            data: response.base64,
            image_three: { uri: response.uri }
          });
        }
        this.image_update(type);
      }
    });
  }

  image_update = async (type) => {
    this.setState({ isLoding: true });
    RNFetchBlob.fetch('POST', api_url + image_upload, {
      'Content-Type': 'multipart/form-data',
    }, [
      {
        name: 'image',
        filename: 'image.png',
        type: 'image/png',
        data: this.state.data
      }
    ]).then((resp) => {
      console.log(resp, this.state.data)
      this.setState({ isLoding: false });
      let data = JSON.parse(resp.data);
      if (data.status == 1 && type == 1) {
        this.setState({ prescription_one: data.result });
      } else if (data.status == 1 && type == 2) {
        this.setState({ prescription_two: data.result });
      } else if (data.status == 1 && type == 3) {
        this.setState({ prescription_three: data.result });
      }
    }).catch((err) => {
      this.setState({ isLoding: false });
      alert("Error on while uploading,Try again");
    })
  }

  add_prescription = async () => {
    Keyboard.dismiss();
    await this.make_images();
    await this.checkValidate();
    if (this.state.validation) {
      this.props.serviceActionPending();
      await axios({
        method: 'post',
        url: api_url + prescription,
        data: { customer_id: global.id, address_id: this.props.address_id, images: this.state.images, customer_notes: this.state.customer_notes, doctor_name: this.state.doctor_name }
      })
        .then(async response => {
          await this.props.serviceActionSuccess(response.data);
          if (response.data.status == 1) {
            this.handleBackButtonClick();
          } else {
            alert('Sorry something went wrong');
          }
        })
        .catch(error => {
          this.props.serviceActionError(error);
        });
    }
  }

  async make_images() {
    let images = [];
    if (this.state.prescription_one) {
      images = this.state.prescription_one;
    }
    if (this.state.prescription_two) {
      images = images + ',' + this.state.prescription_two;
    }
    if (this.state.prescription_three) {
      images = images + ',' + this.state.prescription_three;
    }

    console.log(images, this.state.prescription_one, this.state.image_one)
    this.setState({ images: images });
    return true;

  }
  checkValidate() {
    if (this.state.prescription_one == '' && this.state.prescription_two == '' && this.state.prescription_three == '') {
      this.state.validation = false;
      alert("Please upload images");
      return true;
    } else if (this.props.address_id == undefined) {
      this.state.validation = false;
      alert("Please select address");
      return true;
    } else {
      this.state.validation = true;
      return true;
    }
  }

  render() {

    const { isLoding, error, data, message, status, address, address_id } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Add Prescription</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem header>
              <Text>You can upload maximum 3 images</Text>
            </CardItem>
            <CardItem button>
              <Col>
                <TouchableOpacity onPress={this.select_photo.bind(this, 1)}>
                  <Image
                    style={styles.prescription_image}
                    resizeMode='cover'
                    source={this.state.image_one}
                  />
                </TouchableOpacity>
              </Col>
              <Col>
                <TouchableOpacity onPress={this.select_photo.bind(this, 2)}>
                  <Image
                    style={styles.prescription_image}
                    resizeMode='cover'
                    source={this.state.image_two}
                  />
                </TouchableOpacity>
              </Col>
              <Col>
                <TouchableOpacity onPress={this.select_photo.bind(this, 3)}>
                  <Image
                    style={styles.prescription_image}
                    resizeMode='cover'
                    source={this.state.image_three}
                  />
                </TouchableOpacity>
              </Col>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <TextInput
                style={styles.email}
                placeholder="Doctor Name"
                placeholderTextColor={colors.grey}
                onChangeText={TextInputValue =>
                  this.setState({ doctor_name: TextInputValue })}
              />
            </CardItem>
          </Card>
          <Form>
            <Textarea onChangeText={TextInputValue => this.setState({ customer_notes: TextInputValue })} rowSpan={5} bordered placeholder="Notes" />
          </Form>
          <View style={{ margin: 3 }} />
          <Card>
            <CardItem>
              <Left>
                <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Delivery Address</Text>
              </Left>
              <Right>
                <Text style={{ fontWeight: 'bold', color: colors.theme_fg }} onPress={() => this.select_address()} >{address_id == undefined ? "Select" : "Change"}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Left>
                <Text>{address}</Text>
              </Left>
            </CardItem>
          </Card>
        </Content>
        <Footer style={styles.footer} >
          <TouchableOpacity activeOpacity={1} onPress={() => this.add_prescription()} style={styles.footer_container}>
            <Row>
              <Col style={styles.add_prescription_container} >
                <Text style={styles.add_prescription} >ADD PRESCRIPTION</Text>
              </Col>
            </Row>
          </TouchableOpacity>
        </Footer>
        <Loader visible={isLoding} />
        <Loader visible={this.state.isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.create_prescription.isLoding,
    error: state.create_prescription.error,
    data: state.create_prescription.data,
    message: state.create_prescription.message,
    status: state.create_prescription.status,
    address_id: state.create_prescription.address_id,
    address: state.create_prescription.address,
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPrescription);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  prescription_image: {
    width: 100,
    height: 70,
    alignSelf: 'center',
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_container: {
    width: '100%',
    backgroundColor: colors.theme_fg
  },
  add_prescription_container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  add_prescription: {
    color: colors.theme_bg,
    fontWeight: 'bold'
  },
  email: {
    color: colors.theme_fg,
    fontSize: 16,
    width: '100%'
  },
});
