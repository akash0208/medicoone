import React, { Component } from 'react';
import { StyleSheet, Text, FlatList, Image, View, TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, Left, Body, Right, Title, Button, Footer, Row, Col } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import { api_url, img_url, sub_category } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import { Badge } from 'react-native-elements';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/SubCategoryActions';

class SubCategory extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      data: this.props.route.params.data
    }
    this.getSubCategory();
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  cart = () => {
    this.props.navigation.navigate('Cart');
  }

  getSubCategory = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'post',
      url: api_url + sub_category,
      data: { category_id: this.state.data.id }
    })
      .then(async response => {
        await this.props.serviceActionSuccess(response.data)
      })
      .catch(error => {
        this.props.serviceActionError(error);
        alert('Sorry something went wrong');
      });
  }

  product = async (item) => {
    await this.props.navigation.navigate('Product', { data: item });
  }

  render() {

    const { isLoding, error, data, message, status, cart_count } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >{this.state.data.category_name}</Title>
          </Body>
          <Right style={{ marginRight: 10 }}>
            <TouchableOpacity activeOpacity={1} onPress={this.cart} >
              <Icon style={styles.icon} name='cart' />
              {this.props.cart_count > 0 && <Badge
                status="error"
                containerStyle={{ position: 'absolute', top: -6, right: -6 }}
                value={this.props.cart_count}
              />}
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <View>
            <Image source={{ uri: img_url + this.state.data.category_image }} style={{ width: '100%', height: 180 }} />
          </View>
          <View style={{ margin: 10 }} />
          <List>
            <FlatList data={data}
              renderItem={({ item, index }) =>
                <View style={{ width: '50%', padding: 10 }}>
                  <TouchableOpacity style={{ alignItems: 'center' }} activeOpacity={1} onPress={() => this.product(item, index)} >
                    <Image style={{ width: 120, height: 120, borderRadius: 10 }} source={{ uri: img_url + item.image }} />
                    <View style={{ margin: 5 }} />
                    <Text style={{ color: colors.theme_fg_two, fontSize: 12 }} >{item.sub_category_name}</Text>
                  </TouchableOpacity>
                </View>
              }
              numColumns={2}
            />
          </List>
        </Content>
        {cart_count ?
          <Footer style={styles.footer} >
            <TouchableOpacity activeOpacity={1} onPress={() => this.cart()} style={styles.footer_container}>
              <Row>
                <Col style={styles.view_cart_container} >
                  <Text style={styles.view_cart} >VIEW CART</Text>
                </Col>
              </Row>
            </TouchableOpacity>
          </Footer> : null}
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.sub_category.isLoding,
    error: state.sub_category.error,
    data: state.sub_category.data,
    message: state.sub_category.message,
    status: state.sub_category.status,
    cart_count: state.product.cart_count
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(SubCategory);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  footer_container: {
    width: '100%',
    backgroundColor: colors.theme_fg
  },
  view_cart_container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  view_cart: {
    color: colors.theme_bg,
    fontWeight: 'bold'
  },
  footer: {
    backgroundColor: 'transparent'
  },
});
