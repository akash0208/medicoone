import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Button, Row, Col, List, ListItem } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import * as colors from '../assets/css/Colors';
import ProgressCircle from 'react-native-progress-circle';
import { Divider } from 'react-native-elements';
import Moment from 'moment';
import { tablet } from '../config/Constants';

export default class OrderDetails extends Component<Props> {

  constructor(props) {
      super(props)
      this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
      this.state = {
        data : this.props.route.params.data
      }
  }

  handleBackButtonClick= () => {
      this.props.navigation.goBack(null);
  }

  render() {
    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button transparent onPress={this.handleBackButtonClick}>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Order Details</Title>
          </Body>
          <Right />
        </Header>
        <Content>
            <Row>
              <Body>
                <Text style={styles.order_id}>Order Id - {this.state.data.order_id}</Text>
                <Text style={styles.created_at}>{Moment(this.state.data.created_at).format('DD MMM-YYYY hh:mm')}</Text>
              </Body>
            </Row>
            <Row style={{ margin:20 }} >
              <Body>
                <ProgressCircle
                  percent={ this.state.data.status * 16.666}
                  radius={60}
                  borderWidth={3}
                  color={colors.theme_fg}
                  shadowColor="#e6e6e6"
                  bgColor="#FFFFFF"
                >
                  <View style={{ height:60, width:60 }} >
                    <Image
                      style= {{flex:1 , width: undefined, height: undefined}}
                      source={tablet}
                    />
                  </View>
                </ProgressCircle>
                <Text style={styles.status}>{this.state.data.label_name}</Text>
              </Body>
            </Row>
            <Divider style={styles.order_divider} />
            <Row style={styles.row}>
              <Left>
                <Text style={styles.address_label}>Delivery Address</Text>
                <Text style={styles.address}>{this.state.data.address}</Text>
              </Left>
            </Row>
            <Row style={styles.row}>
              <Left>
                <Text style={styles.address_label}>Payment Mode</Text>
                <Text style={styles.address}>{this.state.data.payment_name}</Text>
              </Left>
            </Row>
            <View style={{ marginTop:10 }} />
            <Divider style={styles.order_divider} />
            <Row style={styles.row}>
              <Left>
                <Text style={styles.your_cloths}>Your items</Text>
              </Left>
            </Row>
            <List>
              {JSON.parse(this.state.data.items).map((row, index) => (
                <ListItem>
                  <Row>
                    <Col style={{ width:40 }} >
                      <Text style={styles.qty} >{row.qty}x</Text>
                    </Col>
                    <Col>
                      <Text>{row.product_name}</Text>
                    </Col>
                    <Col style={{ width:60 }} >
                      <Text>{global.currency}{row.price}</Text>
                    </Col>
                  </Row>
                </ListItem>
              ))}
            </List>
            <Row style={styles.row} >
              <Col>
                <Text>Subtotal</Text>
              </Col>
              <Col style={{ width:60 }} >
                <Text style={{ fontWeight:'bold' }} >{global.currency}{this.state.data.sub_total}</Text>
              </Col>
            </Row>
            <Row style={styles.row} >
              <Col>
                <Text>Discount</Text>
              </Col>
              <Col style={{ width:60 }} >
                <Text style={{ fontWeight:'bold' }} >{global.currency}{this.state.data.discount}</Text>
              </Col>
            </Row>
            <Row style={styles.row} >
              <Col>
                <Text>Delivery Charge</Text>
              </Col>
              <Col style={{ width:60 }} >
                <Text style={{ fontWeight:'bold' }} >{global.currency}{this.state.data.delivery_charge}</Text>
              </Col>
            </Row>
            <View style={{ marginBottom:20 }} />
            <Divider style={styles.order_divider} />
            <Row style={styles.row} >
              <Col>
                <Text style={styles.total_label}>Total</Text>
              </Col>
              <Col style={{ width:60 }} >
                <Text style={styles.total} >{global.currency}{this.state.data.total}</Text>
              </Col>
            </Row>
          </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor:colors.theme_bg
  },
  icon:{
    color:colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title:{
    alignSelf:'center', 
    color:colors.theme_fg,
    alignSelf:'center', 
    fontSize:16, 
    fontWeight:'bold'
  },
  order_id:{
    marginTop:10, 
    fontSize:15, 
    color:colors.theme_fg, 
    fontWeight:'bold'
  },
  created_at:{
    marginTop:5, 
    fontSize:12
  },
  status:{
    marginTop:10, 
    fontSize:13, 
    color:colors.theme_fg, 
    fontWeight:'bold'
  },
  order_divider:{
    backgroundColor: colors.theme_fg_two, 
    width:'90%', 
    alignSelf:'center'
  },
  row:{
    marginLeft:20, 
    marginRight:20, 
    marginTop:10
  },
  address_label:{
    marginTop:10, 
    fontSize:13, 
    color:colors.theme_fg, 
    fontWeight:'bold'
  },
  address:{
    marginTop:5, 
    fontSize:13
  },
  delivery_date_label:{
    marginTop:10, 
    fontSize:13, 
    color:colors.theme_bg,
    fontWeight:'bold'
  },
  delivery_date:{
    marginTop:5, 
    fontSize:13
  },
  your_cloths:{
    marginTop:10, 
    fontSize:13, 
    color:colors.theme_fg, 
    fontWeight:'bold'
  },
  qty:{
    fontSize:15, 
    color:colors.theme_fg, 
    fontWeight:'bold'
  },
  total_label:{
    fontWeight:'bold',
    color:colors.theme_fg
  },
  total:{
    fontWeight:'bold', 
    color:colors.theme_fg
  }
});
