import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { Container, Content, Left, Body, Right, List, ListItem, Button } from 'native-base';
import * as colors from '../assets/css/Colors';
import { Divider } from '../components/GeneralComponents';
import { menus } from '../config/Constants';
import Dialog from "react-native-dialog";

import Icon from 'react-native-vector-icons/Feather'
import IconF from 'react-native-vector-icons/Fontisto'
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native';

export default class More extends Component<Props> {

  constructor(props) {
    super(props)
    this.state = {
      dialogVisible: false,
      isGuest: false
    }

    this.getLoggedInDetails();

  }

  getLoggedInDetails = async () => {
    const user_id = await AsyncStorage.getItem('user_id');
    if (user_id == null) {
      this.setState({ isGuest: true })
    }
  }

  navigate = (route) => {
    if (route == 'Logout') {
      this.showDialog();
    } else if (route == 'AddressList') {
      this.props.navigation.navigate(route, { from: 'More' });
    }
    else {
      this.props.navigation.navigate(route);
    }
  }

  showDialog = () => {
    this.setState({ dialogVisible: true });
  }

  closeDialog = () => {
    this.setState({ dialogVisible: false });
  }

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  }

  handleLogout = async () => {
    await this.closeDialog();
    await this.props.navigation.navigate('Logout');
  }

  _renderEmpty = () => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>You need to login first</Text>
        <TouchableOpacity>
          <Text>LOGIN</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <Container style={styles.container} >
        <View style={styles.header} >
          <Text style={styles.profile_name} >More</Text>
        </View>
        <Divider />
        <Content style={styles.content} >
          <Dialog.Container visible={this.state.dialogVisible}>
            <Dialog.Title>Confirm</Dialog.Title>
            <Dialog.Description>
              Do you want to logout?.
          </Dialog.Description>
            <Dialog.Button label="Yes" onPress={this.handleLogout} />
            <Dialog.Button label="No" onPress={this.handleCancel} />
          </Dialog.Container>

          <List>
            <FlatList
              data={this.state.isGuest ? [] : menus}
              ListEmptyComponent={this._renderEmpty}
              renderItem={({ item, index }) => (
                <ListItem icon onPress={() => this.navigate(item.route)}>
                  <Left>
                    <Button style={styles.icon_button}>
                      {item.menu_name == "Prescriptions" ? <IconF name={"prescription"} style={{ color: colors.theme_bg, fontSize: 16 }} /> :
                        <Icon name={item.icon} style={{ color: colors.theme_bg, fontSize: 16 }} />}
                    </Button>
                  </Left>
                  <Body>
                    <Text style={styles.menu_name} >{item.menu_name}</Text>
                  </Body>
                  <Right />
                </ListItem>
              )}
              keyExtractor={item => item.menu_name}
            />
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.theme_bg_three
  },
  header: {
    backgroundColor: colors.theme_bg,
    padding: 20
  },
  profile_name: {
    fontSize: 16,
    color: colors.theme_fg,
    fontWeight: 'bold'
  },
  content: {
    backgroundColor: colors.theme_fg_three
  },
  icon_button: {
    backgroundColor: colors.theme_fg
  },
  menu_name: {
    fontSize: 16,
    color: colors.theme_fg_two
  }
});

