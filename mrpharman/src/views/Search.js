import React, { Component } from 'react';
import { StyleSheet, Text, ActivityIndicator } from 'react-native';
import { Container, Content, List, ListItem, Body, Col } from 'native-base';
import { api_url, search_products, height_30, no_data } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/ProductActions';
import { SearchBar } from 'react-native-elements';
import { View } from 'react-native';

export default class Search extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      search: '',
      data: [],
      count: undefined,
      on_search: 0
    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  move_product_details = (item) => {
    this.props.navigation.navigate('ProductDetails', { data: item });
  }

  updateSearch = async (search) => {
    this.setState({ search: search, on_search: 1 });
    if (search != "") {
      await axios({
        method: 'post',
        url: api_url + search_products,
        data: { search: search }
      })
        .then(async response => {
          this.setState({ data: response.data.result, count: response.data.count, on_search: 0 });
        })
        .catch(error => {
          this.setState({ data: [], count: undefined, on_search: 0 });
        });
    } else {
      this.setState({ data: [], count: undefined, on_search: 0 });
    }
  };

  render() {

    return (
      <Container>

        <Content>
          <SearchBar
            placeholder="Type Here..."
            onChangeText={this.updateSearch}
            value={this.state.search}
            inputContainerStyle={{ backgroundColor: colors.theme_bg_three, }}
            containerStyle={{ backgroundColor: colors.theme_bg, borderTopWidth: 0, borderBottomWidth: 1, elevation: 5, borderBottomColor: '#ededed' }}
          />
          {this.state.on_search == 1 && <ActivityIndicator size="large" color={colors.theme_fg} />}
          <List>
            {this.state.data.map((item, index) => (
              <ListItem onPress={() => this.move_product_details(item)}>
                <Col>
                  <Text>{item.product_name}</Text>
                  <Text style={{ fontSize: 10 }}>{item.category_name}/{item.sub_category_name}</Text>
                </Col>
              </ListItem>
            ))}
          </List>
          {this.state.count == 0 && <Body style={{ marginTop: height_30 }} >
            <Text>{no_data}</Text>
          </Body>}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg_three
  },
  icon: {
    color: colors.theme_fg_two
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg_two,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  faq_title: {
    color: colors.theme_fg_two,
    fontSize: 15
  }
});
