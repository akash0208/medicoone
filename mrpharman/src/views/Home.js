import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, FlatList, Image, Linking } from 'react-native';
import { StatusBar, Loader } from '../components/GeneralComponents';
import { img_url, api_url, category, app_name } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/HomeActions';
import { productListReset } from '../actions/ProductActions';
import Slideshow from 'react-native-image-slider-show';
import { Button, Icon } from 'react-native-elements';
import { Container, Header, Content, Left, Body, Right, Title, Button as Btn, Footer, Icon as Ic, Row, Col } from 'native-base';

class Home extends Component<Props>{

  constructor(props) {
    super(props)
    this.state = {
      position: 1,
      dataSource: [],
      type: []
    }
    this.Category();
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 3000)
    });
  }

  call_customer(phone_number) {
    Linking.openURL(`tel:+${phone_number}`);
  }

  send_whatsapp(phone_number) {
    Linking.openURL(`https://wa.me/919999160200`)
  }

  product = async (id, service_name) => {
    await this.props.productListReset();
    await this.props.navigation.navigate('Product', { id: id, service_name: service_name });
  }

  move_prescription = async () => {
    await this.props.navigation.navigate('Prescription');
  }

  Category = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'get',
      url: api_url + category
    })
      .then(async response => {
        console.log(response)
        this.setState({ dataSource: response.data.banners, type: response.data.type });
        await this.props.serviceActionSuccess(response.data)
      })
      .catch(error => {
        this.props.serviceActionError(error);
        alert(error);
      });
  }


  move_sub_category = async (item) => {
    await this.props.navigation.navigate('SubCategory', { data: item });
  }

  render() {

    const { isLoding, error, data, message, status } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Row>
            <Col style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
              <View style={{ margin: 3 }} />
              {console.log(global)}
              <Title style={styles.title} >{app_name}</Title>
            </Col>
          </Row>
        </Header>
        <Content>
          <View>
            <Slideshow
              arrowSize={0}
              indicatorSize={0}
              scrollEnabled={true}
              position={this.state.position}
              dataSource={this.state.dataSource} />
          </View>
          <View style={{ padding: 10 }}>
            <Row >
              {/* <Col style={{ width: '20%' }}>
                <Icon name='call' size={30} color="#339966" />
              </Col> */}
              <Col style={{ flex: 2, justifyContent: 'center' }}>
                <Text style={{ fontSize: 24, color: colors.theme_fg, fontWeight: 'bold', alignItems: 'center' }}>Order Now <Text style={{ fontSize: 13 }}>
                  {/* {global.admin_phone} */}
                </Text></Text>
              </Col>
              <Col style={{ flex: 1, alignItems: 'center', justifyContent: 'center', borderEndWidth: 1, borderColor: '#ddd' }}>
                {/* <Text style={{ fontSize: 18, color: colors.theme_fg, fontWeight: 'bold', color: "#339966" }} onPress={() => this.call_customer(global.admin_phone)}>Call</Text> */}
                <Icon
                  name="phone"
                  size={25}
                  type='feather'
                  color={colors.theme_fg}
                  onPress={() => this.call_customer(global.admin_phone)}
                />
              </Col>
              <Col style={{ flex: 0.8, alignItems: 'center', justifyContent: 'center' }}>
                {/* <Text style={{ fontSize: 18, color: colors.theme_fg, fontWeight: 'bold', color: "#339966" }} onPress={() => this.call_customer(global.admin_phone)}>Call</Text> */}
                <Icon
                  name="whatsapp"
                  size={25}
                  type='fontisto'
                  color={colors.theme_fg}
                  onPress={() => this.send_whatsapp(global.admin_phone)}
                />
              </Col>
            </Row>
          </View>
          <View style={{ padding: 10, backgroundColor: colors.theme_bg_three }}>
            <Button
              icon={
                <Icon
                  name="cloud-upload"
                  size={20}
                  type='font-awesome'
                  color={colors.theme_fg}
                  iconStyle={{ marginRight: 10 }}
                />
              }
              onPress={this.move_prescription}
              buttonStyle={{ backgroundColor: colors.theme_bg, borderWidth: 1, borderColor: '#339966' }}
              title="Upload prescription"
              titleStyle={{ color: colors.theme_fg }}
            />
            <View style={{ margin: 10 }} />
            {data.map((item, index) => (
              <View>
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>{item.name}</Text>
                <View style={{ margin: 10 }} />
                <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} >
                  {item.data.map((row, index1) => (
                    <View>
                      <TouchableOpacity activeOpacity={1} onPress={() => this.move_sub_category(row, index)} >
                        <Image style={{ width: 250, height: 130, padding: 20, marginRight: 10, borderRadius: 10 }} source={{ uri: img_url + row.category_image }} />
                        <Text>{row.category_name}</Text>
                        <View style={{ margin: 5 }} />
                      </TouchableOpacity>
                    </View>
                  ))}
                </ScrollView>
              </View>
            ))}
          </View>
        </Content>
        <Loader visible={isLoding} />
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.home.isLoding,
    error: state.home.error,
    data: state.home.data,
    message: state.home.message,
    status: state.home.status,
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data)),
  productListReset: () => dispatch(productListReset())
});


export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  touchable_opacity: {
    backgroundColor: colors.black_layer,
    height: 170,
    alignItems: 'center',
    justifyContent: 'center'
  },
  service_name: {
    color: colors.theme_bg,
    fontSize: 18,
    fontWeight: 'bold'
  },
  description: {
    color: colors.theme_bg,
    paddingLeft: 10,
    paddingRight: 10,
    textAlign: 'center'
  },
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg_two
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    color: colors.theme_fg,
    fontSize: 20,
    fontWeight: 'bold'
  },
  logo_view: {
    height: 50,
    width: 100
  },
});
