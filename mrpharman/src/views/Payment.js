import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Button as Btn } from 'native-base';
import { api_url, place_order, order_generation, get_payment_list } from '../config/Constants';
import Icon from 'react-native-vector-icons/Feather'
import { CommonActions } from '@react-navigation/native';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import axios from 'axios';
import { connect } from 'react-redux';
import { orderServicePending, orderServiceError, orderServiceSuccess, paymentListPending, paymentListError, paymentListSuccess } from '../actions/PaymentActions';
import { reset } from '../actions/CartActions';
import { productReset } from '../actions/ProductActions';
import RazorpayCheckout from 'react-native-razorpay';

var radio_props = [
  { label: 'Cash', value: 1 }
];

class Payment extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.select_payment_method = this.select_payment_method.bind(this);
    this.state = {
      from: this.props.route.params.from,
      prescription_id: this.props.route.params.prescription_id,
      prescription_total: this.props.route.params.prescription_total,
    }
    this.get_payment_modes();
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  get_payment_modes = async () => {
    this.props.paymentListPending();
    await axios({
      method: 'get',
      url: api_url + get_payment_list
    })
      .then(async response => {
        await this.props.paymentListSuccess(response.data);
      })
      .catch(error => {
        this.props.paymentListError(error);
      });
  }

  place_order = async (payment_mode) => {

    this.props.orderServicePending();
    await axios({
      method: 'post',
      url: api_url + place_order,
      data: { customer_id: global.id, payment_mode: payment_mode, delivery_charge: this.props.delivery_charge, address_id: this.props.address, total: this.props.total, discount: this.props.discount, sub_total: this.props.sub_total, promo_id: this.props.promo_id, items: JSON.stringify(Object.values(this.props.items)) }
    })
      .then(async response => {
        await this.props.orderServiceSuccess(response.data);
        await this.move_orders();
      })
      .catch(error => {
        this.props.orderServiceError(error);
      });
  }

  async move_orders() {
    await this.props.reset();
    await this.props.productReset();
    this.props.navigation.navigate('MyOrders');
  }

  place_prescription_order = async (payment_mode) => {
    this.props.orderServicePending();
    await axios({
      method: 'post',
      url: api_url + order_generation,
      data: { customer_id: global.id, payment_mode: payment_mode, prescription_id: this.state.prescription_id }
    })
      .then(async response => {
        await this.props.orderServiceSuccess(response.data);
        await this.move_orders();
      })
      .catch(error => {
        this.props.orderServiceError(error);
      });
  }

  async select_payment_method(payment_mode) {
    if (this.state.from == "prescription") {
      if (payment_mode == 1) {
        await this.place_prescription_order(payment_mode);
      } else {
        var options = {
          currency: global.currency_short_code,
          key: "rzp_test_ejJ5qdbzGQDijf",
          amount: parseInt(this.state.prescription_total * 100),
          name: global.application_name,
          prefill: {
            email: global.email,
            contact: global.phone_number,
            name: global.customer_name
          },
          theme: { color: colors.theme_fg }
        }
        RazorpayCheckout.open(options).then((data) => {
          this.place_prescription_order(payment_mode);
        }).catch((error) => {
          alert('Sorry something went wrong');
        });
      }
    } else {
      if (payment_mode == 1) {
        await this.place_order(payment_mode);
      } else {
        var options = {
          currency: global.currency_short_code,
          key: global.razorpay_key,
          amount: parseInt(this.props.total * 100),
          name: global.application_name,
          prefill: {
            email: global.email,
            contact: global.phone_number,
            name: global.customer_name
          },
          theme: { color: colors.theme_fg }
        }
        RazorpayCheckout.open(options).then((data) => {
          // handle success
          //alert(`Success: ${data.razorpay_payment_id}`);
          this.place_order(payment_mode);
        }).catch((error) => {
          alert('Your Transaction is Declained');
          //alert(JSON.stringify(error));
          // handle failure
          //alert(`Error: ${error.code} | ${error.description}`);
        });
      }
    }
  }

  render() {

    const { isLoding, error, data, message, status, payment_modes } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Btn onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Btn>
          </Left>
          <Body style={styles.heading} >
            <Title style={styles.title} >Payment Mode</Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ padding: 20 }} >
          {payment_modes.map((row, index) => (
            <Text style={styles.name} onPress={() => this.select_payment_method(row.id)} >{row.payment_name}</Text>
          ))}
        </Content>
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.payment.isLoding,
    error: state.payment.error,
    data: state.payment.data,
    message: state.payment.message,
    status: state.payment.status,
    payment_modes: state.payment.payment_modes,
    address: state.cart.address,
    delivery_date: state.cart.delivery_date,
    total: state.cart.total_amount,
    sub_total: state.cart.sub_total,
    delivery_charge: state.cart.delivery_charge,
    discount: state.cart.promo_amount,
    promo_id: state.cart.promo_id,
    items: state.product.cart_items
  };
}

const mapDispatchToProps = (dispatch) => ({
  orderServicePending: () => dispatch(orderServicePending()),
  orderServiceError: (error) => dispatch(orderServiceError(error)),
  orderServiceSuccess: (data) => dispatch(orderServiceSuccess(data)),
  paymentListPending: () => dispatch(paymentListPending()),
  paymentListError: (error) => dispatch(paymentListError(error)),
  paymentListSuccess: (data) => dispatch(paymentListSuccess(data)),
  reset: () => dispatch(reset()),
  productReset: () => dispatch(productReset())
});


export default connect(mapStateToProps, mapDispatchToProps)(Payment);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  radio_style: {
    marginLeft: 20,
    fontSize: 17,
    color: colors.theme_bg,
    fontWeight: 'bold'
  },
  name: {
    padding: 10,
    color: colors.theme_fg,
    fontSize: 16,
    fontWeight: 'bold',
    borderBottomWidth: 1,
    borderColor: '#a3ada6'
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_content: {
    width: '90%'
  },
  place_order: {
    backgroundColor: colors.theme_bg
  }
});
