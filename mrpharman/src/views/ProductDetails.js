import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Button, Row, Col, Footer } from 'native-base';
import * as colors from '../assets/css/Colors';
import Icon from 'react-native-vector-icons/Feather'
import { img_url } from '../config/Constants';
import { Divider } from 'react-native-elements';
import UIStepper from 'react-native-ui-stepper';
import { addToCart } from '../actions/ProductActions';
import { subTotal, deliveryCharge } from '../actions/CartActions';
import { connect } from 'react-redux';
class ProductDetails extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      data: this.props.route.params.data
    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  cart = () => {
    this.props.navigation.navigate('Cart');
  }

  add_to_cart = async (qty, product_id, product_name, price) => {

    let cart_items = this.props.cart_items;
    let old_product_details = cart_items[product_id];
    let sub_total = parseFloat(this.props.sub_total);
    let total_price = parseFloat(qty * price);

    if (old_product_details != undefined && total_price > 0) {
      let final_price = parseFloat(total_price) - parseFloat(old_product_details.price);
      sub_total = parseFloat(sub_total) + parseFloat(final_price);
    } else if (total_price > 0) {
      let final_price = parseFloat(price);
      sub_total = parseFloat(sub_total) + parseFloat(final_price);
    }

    if (qty > 0) {
      let product_data = {
        product_id: product_id,
        product_name: product_name,
        qty: qty,
        price: parseFloat(qty * price)
      }
      cart_items[product_id] = product_data;
      await this.props.addToCart(cart_items);
      await this.props.subTotal(sub_total);
      if (sub_total < global.free_delivery_amount) {
        await this.props.deliveryCharge(global.delivery_charge);
      } else {
        await this.props.deliveryCharge(0);
      }
    } else {
      delete cart_items[product_id];
      sub_total = parseFloat(sub_total) - parseFloat(price);
      await this.props.addToCart(cart_items);
      await this.props.subTotal(sub_total);
      if (sub_total < global.free_delivery_amount) {
        await this.props.deliveryCharge(global.delivery_charge);
      } else {
        await this.props.deliveryCharge(0);
      }
    }
  }

  cart = () => {
    this.props.navigation.navigate('Cart');
  }

  render() {

    const { cart_items, cart_count } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button transparent onPress={this.handleBackButtonClick}>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} onPress={this.cart} >{this.state.data.product_name}</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <View>
            <Image source={{ uri: img_url + this.state.data.image }} style={{ width: '100%', height: 200 }} />
          </View>
          <View style={{ margin: 10 }} />
          <View style={{ padding: 10 }}>
            <Text style={{ fontSize: 15, alignSelf: 'center' }}>{this.state.data.product_name}</Text>
            <Divider style={{ backgroundColor: colors.theme_fg_two, marginTop: 10, marginBottom: 10 }} />
            <Text style={{ fontSize: 15, fontWeight: 'bold', color: colors.theme_fg_two }}>About product</Text>
            <Text style={{ fontSize: 14 }}>{this.state.data.description}</Text>
            <Divider style={{ backgroundColor: colors.theme_fg_two, marginTop: 10, marginBottom: 10 }} />
            <Row>
              <Left>
                <Row>
                  <Text style={{ color: colors.theme_fg, fontSize: 25, fontWeight: 'bold' }}>{global.currency}{this.state.data.price} <Text style={{ fontSize: 10, color: colors.theme_fg_two }}>/ {this.state.data.unit}</Text></Text>
                </Row>
              </Left>
              <Right>
                <UIStepper
                  onValueChange={(value) => { this.add_to_cart(value, this.state.data.id, this.state.data.product_name, this.state.data.price) }}
                  displayValue={true}
                  initialValue={cart_items[this.state.data.id] ? cart_items[this.state.data.id].qty : 0}
                  borderColor={colors.theme_fg}
                  textColor={colors.theme_fg}
                  tintColor={colors.theme_fg}
                />
              </Right>
            </Row>
          </View>
        </Content>
        {cart_count ?
          <Footer style={styles.footer} >
            <TouchableOpacity activeOpacity={1} style={styles.footer_container}>
              <Row>
                <Col onPress={() => this.handleBackButtonClick()} style={styles.view_cart_container} >
                  <Text style={styles.view_cart}>CONTINUE SHOPPING</Text>
                </Col>
                <Col onPress={() => this.cart()} style={styles.view_cart_container} >
                  <Text style={styles.view_cart}>VIEW CART</Text>
                </Col>
              </Row>
            </TouchableOpacity>
          </Footer> : null}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.product.isLoding,
    error: state.product.error,
    data: state.product.data,
    message: state.product.message,
    status: state.product.status,
    cart_items: state.product.cart_items,
    cart_count: state.product.cart_count,
    sub_total: state.cart.sub_total,
    delivery_charge: state.cart.delivery_charge
  };
}

const mapDispatchToProps = (dispatch) => ({
  addToCart: (data) => dispatch(addToCart(data)),
  subTotal: (data) => dispatch(subTotal(data)),
  deliveryCharge: (data) => dispatch(deliveryCharge(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_container: {
    width: '100%',
    backgroundColor: colors.theme_fg
  },
  view_cart_container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  view_cart: {
    color: colors.theme_bg,
    fontWeight: 'bold'
  }
});
