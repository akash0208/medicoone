import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Title, Col } from 'native-base';
import { api_url, my_orders, height_30, no_data, tablet } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import Moment from 'moment';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/MyOrdersActions';
import ProgressCircle from 'react-native-progress-circle';
import { CommonActions } from '@react-navigation/native';

class MyOrders extends Component<Props> {

  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.my_orders();
    });
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  myorders_details = (data) => {
    this.props.navigation.navigate('OrderDetails', { data: data });
  }

  my_orders = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'post',
      url: api_url + my_orders,
      data: { customer_id: global.id }
    })
      .then(async response => {
        await this.props.serviceActionSuccess(response.data)
      })
      .catch(error => {
        this.props.serviceActionError(error);
      });
  }


  render() {
    Moment.locale('en');
    const { isLoding, error, data, message, status } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} />
          <Body style={styles.header_body} >
            <Title style={styles.title} >My Orders</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Loader visible={isLoding} />
          <List>
            {data.map((row, index) => (
              <ListItem onPress={() => this.myorders_details(row)} >
                <Col style={{ width: '30%' }}>
                  <ProgressCircle
                    percent={row.status * 16.666}
                    radius={30}
                    borderWidth={3}
                    color={colors.theme_fg}
                    shadowColor="#e6e6e6"
                  >
                    <View style={{ height: 30, width: 30 }} >
                      <Image
                        style={{ flex: 1, width: undefined, height: undefined }}
                        source={tablet}
                      />
                    </View>
                  </ProgressCircle>
                </Col>
                <Col style={{ width: '50%' }} >
                  <Text style={styles.order_id}>Order Id : {row.order_id}</Text>
                  <View style={{ margin: 1 }} />
                  <Text style={{ fontSize: 10 }} >{Moment(row.created_at).format('DD MMM-YYYY hh:mm')}</Text>
                  <Text style={{ color: colors.theme_fg }} >{row.label_name}</Text>
                </Col>
                <Col>
                  <Text style={styles.total} >{global.currency}{row.total}</Text>
                </Col>
              </ListItem>
            ))}
          </List>
          {data.length == 0 ? <Body style={{ marginTop: height_30 }} >
            <Text style={{ color: '#999' }}>{no_data}</Text>
          </Body> : null}
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.myorders.isLoding,
    error: state.myorders.error,
    data: state.myorders.data,
    message: state.myorders.message,
    status: state.myorders.status,
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  order_id: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.theme_fg
  },
  total: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.theme_fg
  }
});
