import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, ScrollView, Image, TouchableOpacity, Keyboard } from 'react-native';
import { Button } from 'react-native-elements';
import { api_url, profile, height_35, height_40, profile_picture } from '../config/Constants';
import { StatusBar, Loader } from '../components/GeneralComponents';
import { Header, Left, Body, Right, Title, Button as Btn } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import * as colors from '../assets/css/Colors';
import axios from 'axios';
import { connect } from 'react-redux';
import { editServiceActionPending, editServiceActionError, editServiceActionSuccess, updateServiceActionPending, updateServiceActionError, updateServiceActionSuccess, updateProfilePicture } from '../actions/ProfileActions';
import { launchImageLibrary } from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';

const options = {
  mediaType: 'photo',
  includeBase64: true,
};

class Profile extends Component<Props>{

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      profile_picture: '',
      customer_name: '',
      phone_number: '',
      email: '',
      password: '',
      validation: true,
      data: ''
    }

  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  async componentDidMount() {
    await this.get_profile();
  }

  get_profile = async () => {
    this.props.editServiceActionPending();
    await axios({
      method: 'get',
      url: api_url + profile + '/' + global.id + '/edit'
    })
      .then(async response => {
        await this.props.editServiceActionSuccess(response.data);
        await this.setState({ customer_name: this.props.data.customer_name, email: this.props.data.email, phone_number: this.props.data.phone_number, profile_picture: this.props.profile_picture })
      })
      .catch(error => {
        this.showSnackbar("Sorry something went wrong!");
        this.props.editServiceActionError(error);
      });
  }

  update_profile = async () => {
    Keyboard.dismiss();
    await this.checkValidate();
    if (this.state.validation) {
      this.props.updateServiceActionPending();
      await axios({
        method: 'patch',
        url: api_url + profile + '/' + global.id,
        data: { customer_name: this.state.customer_name, phone_number: this.state.phone_number, email: this.state.email, password: this.state.password }
      })
        .then(async response => {
          alert('Successfully updated');
          await this.props.updateServiceActionSuccess(response.data);
          await this.saveData();
        })
        .catch(error => {
          this.props.updateServiceActionError(error);
        });
    }
  }

  saveData = async () => {
    if (this.props.status == 1) {
      try {
        await AsyncStorage.setItem('user_id', this.props.data.id.toString());
        await AsyncStorage.setItem('customer_name', this.props.data.customer_name.toString());
        global.id = await this.props.data.id;
        global.customer_name = await this.props.data.customer_name;
        await this.showSnackbar("Profile updated Successfully");
        await this.setState({ password: '' });
      } catch (e) {

      }
    } else {
      alert(this.props.message);
    }
  }

  checkValidate() {
    if (this.state.email == '' || this.state.phone_number == '' || this.state.customer_name == '') {
      this.state.validation = false;
      this.showSnackbar("Please fill all the fields.");
      return true;
    }
  }

  select_photo() {
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };
        this.setState({
          data: response.data
        });
        this.props.updateProfilePicture(source);
        this.profileimageupdate();
      }
    });
  }

  profileimageupdate = async () => {
    RNFetchBlob.fetch('POST', api_url + profile_picture, {
      'Content-Type': 'multipart/form-data',
    }, [
      {
        name: 'profile_picture',
        filename: 'image.png',
        type: 'image/png',
        data: this.state.data
      },
      {
        name: 'customer_id',
        data: global.id.toString()
      }
    ]).then((resp) => {
      this.showSnackbar("Updated Successfully");
    }).catch((err) => {
      this.showSnackbar("Error on while uploading,Try again");
    })
  }


  showSnackbar(msg) {
    Snackbar.show({
      title: msg,
      duration: Snackbar.LENGTH_SHORT,
    });
  }

  render() {

    const { isLoding, error, data, profile_picture, message, status } = this.props

    return (
      <ScrollView keyboardShouldPersistTaps='always' style={styles.page_background}>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Btn onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Btn>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Profile</Title>
          </Body>
          <Right />
        </Header>
        {/* Header section */}
        <View style={styles.container}>

          <View style={styles.header_section} >
            <TouchableOpacity onPress={this.select_photo.bind(this)}>
              <Image
                style={styles.profile_image}
                resizeMode='cover'
                source={profile_picture}
              />
            </TouchableOpacity>
            <View>
              <Text style={styles.profile_name} >{this.state.customer_name}</Text>
            </View>
          </View>

          {/* Body section */}
          <View style={styles.body_section} >
            <View style={styles.input}>
              <TextInput
                style={styles.text_input}
                placeholder="USERNAME"
                placeholderTextColor={colors.theme_fg}
                value={this.state.customer_name}
                onChangeText={TextInputValue =>
                  this.setState({ customer_name: TextInputValue })}
              />
            </View>
            <View style={styles.input}>
              <TextInput
                style={styles.text_input}
                placeholder="PHONE"
                placeholderTextColor={colors.theme_fg}
                keyboardType="phone-pad"
                value={this.state.phone_number}
                onChangeText={TextInputValue =>
                  this.setState({ phone_number: TextInputValue })}
              />
            </View>
            <View style={styles.input}>
              <TextInput
                style={styles.text_input}
                placeholder="EMAIL ADDRESS"
                placeholderTextColor={colors.theme_fg}
                keyboardType="email-address"
                value={this.state.email}
                onChangeText={TextInputValue =>
                  this.setState({ email: TextInputValue })}
              />
            </View>
            <View style={styles.input} >
              <TextInput
                style={styles.text_input}
                placeholder="PASSWORD"
                placeholderTextColor={colors.theme_fg}
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={TextInputValue =>
                  this.setState({ password: TextInputValue })}
              />
            </View>
          </View>

          {/* Footer section */}
          <View style={styles.footer_section} >
            <View style={styles.input} >
              <Button
                title="Update"
                onPress={this.update_profile}
                buttonStyle={styles.btn_style}
                titleStyle={{ color: colors.theme_bg }}
              />
            </View>
          </View>
        </View>
        <Loader visible={isLoding} />
      </ScrollView>
    )
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.profile.isLoding,
    message: state.profile.message,
    status: state.profile.status,
    data: state.profile.data,
    profile_picture: state.profile.profile_picture
  };
}

const mapDispatchToProps = (dispatch) => ({
  editServiceActionPending: () => dispatch(editServiceActionPending()),
  editServiceActionError: (error) => dispatch(editServiceActionError(error)),
  editServiceActionSuccess: (data) => dispatch(editServiceActionSuccess(data)),
  updateServiceActionPending: () => dispatch(updateServiceActionPending()),
  updateServiceActionError: (error) => dispatch(updateServiceActionError(error)),
  updateServiceActionSuccess: (data) => dispatch(updateServiceActionSuccess(data)),
  updateProfilePicture: (data) => dispatch(updateProfilePicture(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  page_background: {
    backgroundColor: colors.theme_bg_three
  },
  header_section: {
    width: '100%',
    height: height_35,
    backgroundColor: colors.theme_fg,
    alignItems: 'center',
    justifyContent: 'center'
  },
  profile_image: {
    width: 90,
    height: 90,
    borderRadius: 50,
    borderColor: colors.theme_bg,
    backgroundColor: colors.theme_fg,
    borderWidth: 1
  },
  profile_name: {
    color: colors.theme_bg,
    marginTop: 10,
    fontSize: 20,
    fontWeight: 'bold'
  },
  body_section: {
    width: '100%',
    height: height_40,
    backgroundColor: colors.theme_bg_three,
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    height: 40,
    width: '80%',
    marginTop: 10
  },
  text_input: {
    borderColor: colors.theme_fg,
    color: colors.theme_fg,
    borderWidth: 1,
    padding: 10,
    borderRadius: 5
  },
  footer_section: {
    width: '100%',
    alignItems: 'center',
    marginBottom: 10
  },
  btn_style: {
    backgroundColor: colors.theme_fg
  },
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  }
});