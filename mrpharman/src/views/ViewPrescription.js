import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Alert } from 'react-native';
import { Container, Header, Content, Left, Body, Right, Title, Button as Btn, Card, CardItem, Col, Footer, Row } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import { api_url, img_url, reject_order } from '../config/Constants';
import * as colors from '../assets/css/Colors';
import { Loader } from '../components/GeneralComponents';
import { Divider, Button } from 'react-native-elements';
import axios from 'axios';
import { connect } from 'react-redux';
import { serviceActionPending, serviceActionError, serviceActionSuccess } from '../actions/ViewPrescriptionActions';

class ViewPrescription extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.reject_order = this.reject_order.bind(this);
    this.state = {
      data: this.props.route.params.data
    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  accept_order = () => {
    this.props.navigation.navigate('Payment', { from: 'prescription', prescription_id: this.state.data.id, prescription_total: this.state.data.total });
  }

  reject_order = async () => {
    this.props.serviceActionPending();
    await axios({
      method: 'post',
      url: api_url + reject_order,
      data: { prescription_id: this.state.data.id }
    })
      .then(async response => {
        await this.props.serviceActionSuccess(response.data)
        if (response.data.status == 1) {
          Alert.alert(
            "Rejected",
            "This prescription successfully rejected.",
            [
              { text: "OK", onPress: () => this.props.navigation.goBack(null) }
            ],
            { cancelable: false }
          );

        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        this.props.serviceActionError(error);
      });
  }

  render() {

    const { isLoding, error, data, message, status } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Btn onPress={this.handleBackButtonClick} transparent>
              <Icon style={styles.icon} name='arrow-left' />
            </Btn>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >View Prescription</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Card>
            <CardItem header>
              <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Prescription images</Text>
            </CardItem>
            <CardItem>
              {this.state.data.images.map((row, index) => (
                <Col>
                  <Image
                    style={styles.prescription_image}
                    resizeMode='cover'
                    source={{ uri: img_url + row }}
                  />
                </Col>
              ))}
            </CardItem>
          </Card>
          {this.state.data.customer_notes &&
            <Card>
              <CardItem header>
                <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Your Notes</Text>
              </CardItem>
              <CardItem>
                <Text>{this.state.data.customer_notes}</Text>
              </CardItem>
            </Card>
          }

          <Card>
            <CardItem header>
              <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Doctor Name</Text>
            </CardItem>
            <CardItem>
              <Text>{this.state.data.doctor_name}</Text>
            </CardItem>
          </Card>

          <Card>
            <CardItem header>
              <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Delivery Address</Text>
            </CardItem>
            <CardItem>
              <Text>{this.state.data.address}</Text>
            </CardItem>
          </Card>
          {this.state.data.status != 1 &&
            <Card>
              <CardItem header>
                <Text style={{ fontWeight: 'bold', color: colors.theme_fg_two }}>Cart Items</Text>
              </CardItem>
              {this.state.data.items.map((row, index) => (
                <CardItem>
                  <Row>
                    <Col style={{ width: 40 }} >
                      <Text style={styles.qty} >{row.qty}x</Text>
                    </Col>
                    <Col style={{ width: '70%' }}>
                      <Text>{row.product_name}</Text>
                    </Col>
                    <Col style={{ width: 50 }} >
                      <Text>{global.currency}{row.price}</Text>
                    </Col>
                  </Row>
                </CardItem>
              ))}
              <Divider style={styles.order_divider} />
              <Row style={styles.row} >
                <Col>
                  <Text>Subtotal</Text>
                </Col>
                <Col style={{ width: 50 }} >
                  <Text>{global.currency}{this.state.data.total}</Text>
                </Col>
              </Row>
              <Row style={styles.row} >
                <Col>
                  <Text>Delivery Charge</Text>
                </Col>
                <Col style={{ width: 50 }} >
                  <Text>{global.currency}{this.state.data.delivery_charge}</Text>
                </Col>
              </Row>
              <View style={{ marginBottom: 20 }} />
              <Divider style={styles.order_divider} />
              <Row style={styles.row} >
                <Col>
                  <Text style={styles.total_label}>Total</Text>
                </Col>
                <Col style={{ width: 50 }} >
                  <Text style={{ fontWeight: 'bold' }} >{global.currency}{this.state.data.total}</Text>
                </Col>
              </Row>
              <View style={{ marginBottom: 20 }} />
            </Card>
          }
        </Content>
        {this.state.data.status == 7 &&
          <Footer style={styles.footer} >
            <Row style={{ padding: 10 }}>
              <Col>
                <Button
                  title="Reject"
                  onPress={this.reject_order}
                  buttonStyle={{ backgroundColor: colors.red }}
                />
              </Col>
              <Col style={{ width: 10 }} />
              <Col>
                <Button
                  title="Accept"
                  onPress={this.accept_order}
                  buttonStyle={{ backgroundColor: colors.theme_fg }}
                />
              </Col>
            </Row>
          </Footer>
        }
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.view_prescription.isLoding,
    error: state.view_prescription.error,
    data: state.view_prescription.data,
    message: state.view_prescription.message,
    status: state.view_prescription.status,
  };
}

const mapDispatchToProps = (dispatch) => ({
  serviceActionPending: () => dispatch(serviceActionPending()),
  serviceActionError: (error) => dispatch(serviceActionError(error)),
  serviceActionSuccess: (data) => dispatch(serviceActionSuccess(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(ViewPrescription);

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  prescription_image: {
    width: 100,
    height: 70,
    alignSelf: 'center',
    borderColor: colors.theme_bg_three,
    borderWidth: 1
  },
  row: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10
  },
  order_divider: {
    backgroundColor: colors.theme_fg_two,
    width: '90%',
    alignSelf: 'center'
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_container: {
    width: '100%',
    backgroundColor: colors.theme_bg
  },
  btn_cart: {
    color: colors.theme_fg_three,
    fontWeight: 'bold',
    backgroundColor: colors.theme_bg

  }
});
