import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Container, Header, Content, Card, CardItem, Left, Body, Right, Title, Button } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import * as colors from '../assets/css/Colors';

export default class FaqDetails extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      data: this.props.route.params.data
    }
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  render() {
    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Button transparent onPress={this.handleBackButtonClick}>
              <Icon style={styles.icon} name='arrow-left' />
            </Button>
          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >{this.state.data.question}</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Body>
                <Text>{this.state.data.answer}</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
});
