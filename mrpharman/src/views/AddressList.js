import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Footer, Col } from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import { Button } from 'react-native-elements';
import axios from 'axios';
import { connect } from 'react-redux';
import { Loader } from '../components/GeneralComponents';
import { listServiceActionPending, listServiceActionError, listServiceActionSuccess, deleteServiceActionPending, deleteServiceActionError, deleteServiceActionSuccess } from '../actions/AddressListActions';
import { selectAddress } from '../actions/CartActions';
import { prescriptionSelectAddress } from '../actions/CreatePrescriptionActions';
import { api_url, address_list, address_delete, img_url, height_30, no_data } from '../config/Constants';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import * as colors from '../assets/css/Colors';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

class AddressList extends Component<Props> {

  constructor(props) {
    super(props)
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      dialogVisible: false,
      deleting_address: 0,
      from: this.props.route.params.from
    }
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.address_list();
    });
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  address_list = async () => {
    this.setState({ dialogVisible: false })
    this.props.deleteServiceActionPending();
    await axios({
      method: 'post',
      url: api_url + address_list,
      data: { customer_id: global.id }
    })
      .then(async response => {
        await this.props.deleteServiceActionSuccess(response.data);
      })
      .catch(error => {
        this.props.deleteServiceActionError(error);
      });
  }

  address_delete = async () => {
    this.setState({ dialogVisible: false })
    this.props.deleteServiceActionPending();
    await axios({
      method: 'post',
      url: api_url + address_delete,
      data: { customer_id: global.id, address_id: this.state.deleting_address }
    })
      .then(async response => {
        await this.props.deleteServiceActionSuccess(response.data);
        await this.setState({ deleting_address: 0 });

      })
      .catch(error => {
        this.props.deleteServiceActionError(error);
      });
  }

  open_popup(id) {
    this.setState({ dialogVisible: true, deleting_address: id })
  }

  close_popup() {
    this.setState({ dialogVisible: false, deleting_address: 0 })
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack(null);
  }

  add_address = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
      .then(data => {
        this.props.navigation.navigate('Address', { id: 0 });
      }).catch(err => {
        alert('Please enable location');
      });
  }

  edit_address = (id) => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
      .then(data => {
        this.props.navigation.navigate('Address', { id: id });
      }).catch(err => {
        alert('Please enable location');
      });
  }

  select_address = async (id) => {
    await this.props.selectAddress(id);
    await this.props.navigation.navigate('Payment', { from: 'address' });
  }

  select_address_prescription = async (id, address) => {
    let data = { id: id, address: address }
    await this.props.prescriptionSelectAddress(data);
    await this.props.navigation.goBack(null);
  }

  render() {

    const { isLoding, error, data, message, status, address_count } = this.props

    const address_list = data.map((row, key) => {
      return (
        <View style={styles.address_content} >
          <View style={{ flexDirection: 'row' }} >
            <Left>
              <Text style={styles.address_title} >Address #{key + 1}</Text>
            </Left>
          </View>
          <View style={styles.static_map} >
            {console.log(row.static_map)}
            <Image
              style={{ flex: 1, width: undefined, height: undefined }}
              source={{ uri: img_url + row.static_map }}
            />
          </View>
          <View style={{ flexDirection: 'row' }} >
            <Left>
              <Text style={styles.address} >
                {row.address}
              </Text>
            </Left>
          </View>
          <View style={styles.address_footer} >
            <Col style={{ width: '25%' }} >
              <Text onPress={this.edit_address.bind(this, row.id)} style={styles.btn} >EDIT</Text>
            </Col>
            <Col style={{ width: '25%' }}>
              <Text onPress={this.open_popup.bind(this, row.id)} style={styles.btn} >DELETE</Text>
            </Col>
            {this.state.from == 'cart' &&
              <Col style={{ width: '25%' }}>
                <Text onPress={this.select_address.bind(this, row.id)} style={styles.btn} >SELECT</Text>
              </Col>
            }
            {this.state.from == 'prescription' &&
              <Col style={{ width: '25%' }}>
                <Text onPress={this.select_address_prescription.bind(this, row.id, row.address)} style={styles.btn} >SELECT</Text>
              </Col>
            }
          </View>
        </View>
      )
    })

    return (
      <Container style={{ backgroundColor: colors.theme_bg_two }} >
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >
            <Icon onPress={this.handleBackButtonClick} style={styles.icon} name='arrow-left' />
          </Left>
          <Body style={styles.header_body} >
            <Title style={{ alignSelf: 'center', color: colors.theme_fg, fontSize: 16, fontWeight: 'bold' }} >MANAGE ADDRESSES</Title>
          </Body>
          <Right />
        </Header>
        <ScrollView>
          <View style={styles.container} >
            <View>
              <Text style={styles.your_address} >YOUR ADDRESSES</Text>
            </View>
            {console.log(address_list)}
            {address_count == 0 ? <View style={{ marginTop: height_30 }} >
              <Text>{no_data}</Text>
            </View> : address_list}
          </View>
        </ScrollView>
        <Footer style={styles.footer} >
          <View style={styles.footer_content}>
            <Button
              title="ADD NEW ADDRESS"
              titleStyle={{ color: colors.theme_bg }}
              onPress={this.add_address}
              buttonStyle={styles.add_address}
            />
          </View>
        </Footer>
        <Loader visible={isLoding} />
        <ConfirmDialog
          title="Confirm Dialog"
          message="Are you sure about that?"
          animationType="fade"
          visible={this.state.dialogVisible}
          onTouchOutside={() => this.setState({ dialogVisible: false })}
          positiveButton={{
            title: "YES",
            onPress: this.address_delete,
            titleStyle: {
              color: colors.theme_fg
            },
          }}
          negativeButton={{
            title: "NO",
            onPress: () => this.setState({ dialogVisible: false }),
            titleStyle: {
              color: colors.theme_fg
            },
          }}
        />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoding: state.address_list.isLoding,
    message: state.address_list.isLoding,
    status: state.address_list.isLoding,
    data: state.address_list.data,
    address_count: state.address_list.address_count
  };
}

const mapDispatchToProps = (dispatch) => ({
  listServiceActionPending: () => dispatch(listServiceActionPending()),
  listServiceActionError: (error) => dispatch(listServiceActionError(error)),
  listServiceActionSuccess: (data) => dispatch(listServiceActionSuccess(data)),
  deleteServiceActionPending: () => dispatch(deleteServiceActionPending()),
  deleteServiceActionError: (error) => dispatch(deleteServiceActionError(error)),
  deleteServiceActionSuccess: (data) => dispatch(deleteServiceActionSuccess(data)),
  selectAddress: (data) => dispatch(selectAddress(data)),
  prescriptionSelectAddress: (data) => dispatch(prescriptionSelectAddress(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressList);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  address_content: {
    width: '100%',
    padding: 20,
    backgroundColor: colors.theme_bg_three,
    marginBottom: 10
  },
  address_title: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.theme_fg_two
  },
  static_map: {
    height: 100,
    width: '100%',
    marginTop: 10
  },
  address: {
    fontSize: 15,
    marginTop: 5
  },
  address_footer: {
    flexDirection: 'row',
    marginTop: 10
  },
  btn: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.theme_fg
  },
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg,
    fontSize: 20
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: '#202028',
    fontSize: 16,
    fontWeight: 'bold'
  },
  your_address: {
    fontSize: 12,
    margin: 10
  },
  footer: {
    backgroundColor: colors.theme_bg_three
  },
  footer_content: {
    width: '90%',
    justifyContent: 'center'
  },
  add_address: {
    backgroundColor: colors.theme_fg
  }
});

