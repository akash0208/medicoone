import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Row, Footer, Col, List, ListItem } from 'native-base';
import { Button, Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Fontisto';
import { Loader } from '../components/GeneralComponents';
import { connect } from 'react-redux';
import { subTotal, total, calculatePricing, selectDate } from '../actions/CartActions';
import Snackbar from 'react-native-snackbar';
import * as colors from '../assets/css/Colors';
import { height_30, no_data } from '../config/Constants';
import DateTimePicker from "react-native-modal-datetime-picker";

class Cart extends Component<Props> {

  constructor(props) {
    super(props)
    this.state = {
      deliveryDatePickerVisible: false,
      minDate: new Date()
    }
  }

  async componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.calculate_total();
    });

    const tomorrow = new Date(this.state.minDate)
    tomorrow.setDate(tomorrow.getDate() + 1)
    this.state.minDate = tomorrow;
  }
  componentWillUnmount() {
    this._unsubscribe();
  }

  showDeliveryDatePicker = () => {
    this.setState({ deliveryDatePickerVisible: true });
  };

  hideDeliveryDatePicker = () => {
    this.setState({ deliveryDatePickerVisible: false });
  };

  handleDeliveryDatePicked = async (date) => {
    this.setState({ deliveryDatePickerVisible: false })
    var d = new Date(date);
    let delivery_date = d.getDate() + '/' + ("0" + (d.getMonth() + 1)).slice(-2) + '/' + d.getFullYear();
    await this.props.selectDate(delivery_date);
  };


  calculate_total() {
    this.props.calculatePricing();
    promo = this.props.promo;
    if (promo == undefined) {
      let total = parseFloat(parseFloat(parseFloat(this.props.sub_total).toFixed(2)) + parseFloat(parseFloat(this.props.delivery_charge).toFixed(2))).toFixed(2);
      this.props.total({ promo_amount: 0, sub_total: parseFloat(this.props.sub_total).toFixed(2), delivery_charge: parseFloat(this.props.delivery_charge).toFixed(2), total: total });
    } else if (promo.promo_type != undefined) {
      let total_with_delivery = parseFloat(parseFloat(this.props.sub_total).toFixed(2) + parseFloat(parseFloat(this.props.delivery_charge).toFixed(2))).toFixed(2);
      let discount = parseFloat(parseFloat(promo.discount / 100).toFixed(2) * parseFloat(this.props.sub_total).toFixed(2)).toFixed(2);
      let total = parseFloat(parseFloat(total_with_delivery).toFixed(2) - parseFloat(discount).toFixed(2) + parseFloat(parseFloat(this.props.delivery_charge).toFixed(2))).toFixed(2);
      this.props.total({ promo_amount: parseFloat(discount).toFixed(2), total: parseFloat(this.props.sub_total).toFixed(2) - parseFloat(discount).toFixed(2) + parseFloat(this.props.delivery_charge).toFixed(2), total: total });
    } else {
      let total_with_delivery = parseFloat(parseFloat(this.props.sub_total).toFixed(2) + parseFloat(this.props.delivery_charge).toFixed(2)).toFixed(2);
      let discount = parseFloat(parseFloat(promo.discount / 100).toFixed(2) * parseFloat(this.props.sub_total).toFixed(2)).toFixed(2);
      let total = parseFloat(parseFloat(total_with_delivery).toFixed(2) - parseFloat(discount).toFixed(2)).toFixed(2);
      this.props.total({ promo_amount: parseFloat(discount).toFixed(2), total: parseFloat(this.props.sub_total).toFixed(2) - parseFloat(discount).toFixed(2) + parseFloat(this.props.delivery_charge).toFixed(2), total: total });

    }

  }



  address_list = () => {
    this.props.navigation.navigate('AddressList');
  }

  select_address = () => {
    this.props.navigation.navigate('AddressList', { from: 'cart' });
  }

  showSnackbar(msg) {
    Snackbar.show({
      title: msg,
      duration: Snackbar.LENGTH_SHORT,
    });
  }

  promo = () => {
    this.props.navigation.navigate('Promo');
  }

  render() {

    const { isLoding, cart_items, cart_count, sub_total, total_amount, promo_amount, promo, delivery_date, delivery_charge } = this.props

    return (
      <Container>
        <Header androidStatusBarColor={colors.theme_fg} style={styles.header} >
          <Left style={{ flex: 1 }} >

          </Left>
          <Body style={styles.header_body} >
            <Title style={styles.title} >Cart</Title>
          </Body>
          <Right />
        </Header>
        {cart_count > 0 &&
          <ScrollView>
            <View>
              <Divider style={{ backgroundColor: colors.theme_fg_two }} />
              <Row style={{ padding: 10 }} >
                <Left>
                  <Text>Your items</Text>
                </Left>
              </Row>
              <List>
                {Object.keys(cart_items).map(function (key) {
                  return <ListItem>
                    <Row>
                      <Col style={{ width: 40 }} >
                        <Text style={styles.qty} >{cart_items[key].qty}x</Text>
                      </Col>
                      <Col>
                        <Text>{cart_items[key].product_name}</Text>
                      </Col>
                      <Col style={{ width: 50 }} >
                        <Text>{global.currency}{cart_items[key].price}</Text>
                      </Col>
                    </Row>
                  </ListItem>
                })}
              </List>
              {promo === undefined ?
                <Row style={{ padding: 20 }} >
                  <Col style={{ width: 50 }} >
                    <Icon style={{ color: colors.theme_fg }} size={30} name='shopping-sale' />
                  </Col>
                  <Col>
                    <Text style={{ fontSize: 12 }} >No promotion applied.Choose your promotion here.</Text>
                    <Text onPress={() => this.promo()} style={styles.choose_promotion}>Choose promotion</Text>
                  </Col>
                </Row> :
                <Row style={{ padding: 20 }} >
                  <Col style={{ width: 50 }} >
                    <Icon style={{ color: colors.theme_fg }} size={30} name='shopping-sale' />
                  </Col>
                  <Col>
                    <Text style={styles.promotion_applied} >Promotion applied</Text>
                    <Text style={{ fontSize: 12 }} >You are saving {global.currency}{promo_amount}</Text>
                    <Text onPress={() => this.promo()} style={styles.change_promo}>Change promo</Text>
                  </Col>
                </Row>
              }
              <Divider style={{ backgroundColor: colors.theme_fg_two }} />
              <Row style={styles.sub_total} >
                <Col>
                  <Text>Subtotal</Text>
                </Col>
                <Col style={{ width: 60 }} >
                  <Text style={{ fontWeight: 'bold' }} >{global.currency}{parseFloat(sub_total).toFixed(2)}</Text>
                </Col>
              </Row>
              <Row style={styles.discount} >
                <Col>
                  <Text>Discount</Text>
                </Col>
                <Col style={{ width: 60 }} >
                  <Text style={{ fontWeight: 'bold' }} >{global.currency}{promo_amount}</Text>
                </Col>
              </Row>
              <Row style={styles.sub_total} >
                <Col>
                  <Text>Delivery Charge</Text>
                </Col>
                <Col style={{ width: 60 }} >
                  <Text style={{ fontWeight: 'bold' }} >{global.currency}{delivery_charge}</Text>
                </Col>
              </Row>
              <Row style={styles.total} >
                <Col>
                  <Text>Total</Text>
                </Col>
                <Col style={{ width: 60 }} >
                  <Text style={styles.total_amount} >{global.currency}{total_amount}</Text>
                </Col>
              </Row>
              <Divider style={{ backgroundColor: colors.theme_fg_two }} />
              <Row style={styles.delivery_date} >
                <Col>
                  <Button
                    title="Choose Expected Delivery Date"
                    type="outline"
                    buttonStyle={{ borderColor: colors.theme_bg, backgroundColor: colors.theme_fg }}
                    titleStyle={{ color: colors.theme_bg }}
                    onPress={this.showDeliveryDatePicker}
                  />
                  <DateTimePicker
                    isVisible={this.state.deliveryDatePickerVisible}
                    onConfirm={this.handleDeliveryDatePicked}
                    onCancel={this.hideDeliveryDatePicker}
                    minimumDate={this.state.minDate}
                    mode='date'
                  />
                </Col>
              </Row>
              {delivery_date != undefined &&
                <Row style={{ justifyContent: 'center' }} >
                  <Text style={styles.delivery_date_text}>{delivery_date}</Text>
                </Row>
              }
              <Footer style={styles.footer} >
                <View style={styles.footer_content}>
                  <Button
                    onPress={this.select_address}
                    title="Select Address"
                    titleStyle={{ color: colors.theme_bg, }}
                    buttonStyle={styles.select_address}
                  />
                </View>
              </Footer>
            </View>
          </ScrollView>
        }
        {cart_count == undefined &&
          <Body>
            <Text style={{ marginTop: height_30, color: '#999' }}>{"Your cart is empty!"}</Text>
          </Body>
        }
        <Loader visible={isLoding} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    cart_items: state.product.cart_items,
    cart_count: state.product.cart_count,
    sub_total: state.cart.sub_total,
    delivery_charge: state.cart.delivery_charge,
    promo: state.cart.promo,
    total_amount: state.cart.total_amount,
    promo_amount: state.cart.promo_amount,
    isLoding: state.cart.isLoding,
    delivery_date: state.cart.delivery_date
  };
}

const mapDispatchToProps = (dispatch) => ({
  subTotal: (data) => dispatch(subTotal(data)),
  total: (data) => dispatch(total(data)),
  calculatePricing: () => dispatch(calculatePricing()),
  selectDate: (data) => dispatch(selectDate(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(Cart);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.theme_bg_two,
  },
  header: {
    backgroundColor: colors.theme_bg
  },
  icon: {
    color: colors.theme_fg
  },
  header_body: {
    flex: 3,
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    color: colors.theme_fg,
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold'
  },
  qty: {
    fontSize: 15,
    color: colors.theme_fg,
    fontWeight: 'bold'
  },
  promotion_applied: {
    fontSize: 15,
    color: colors.theme_fg,
    fontWeight: 'bold'
  },
  choose_promotion: {
    color: colors.theme_fg,
    fontWeight: 'bold'
  },
  change_promo: {
    color: colors.theme_fg,
    fontSize: 13
  },
  sub_total: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10
  },
  discount: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10
  },
  total: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  total_amount: {
    fontWeight: 'bold',
    color: colors.theme_fg_two
  },
  delivery_date: {
    padding: 20,
    justifyContent: 'center'
  },
  delivery_date_text: {
    color: colors.theme_fg,
    marginBottom: 20
  },
  footer: {
    backgroundColor: 'transparent'
  },
  footer_content: {
    width: '90%'
  },
  select_address: {
    backgroundColor: colors.theme_fg
  }
});

