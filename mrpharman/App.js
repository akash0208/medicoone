import React, { Fragment } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { fromRight } from 'react-navigation-transitions';
import Icon from 'react-native-vector-icons/Feather';
import * as colors from './src/assets/css/Colors';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

/* Screens */
import Splash from './src/views/Splash';
import Home from './src/views/Home';
import SubCategory from './src/views/SubCategory';
import Product from './src/views/Product';
import ProductDetails from './src/views/ProductDetails';
import Promo from './src/views/Promo';
import MyOrders from './src/views/MyOrders';
import Search from './src/views/Search';
import OrderDetails from './src/views/OrderDetails';
import Cart from './src/views/Cart';
import Profile from './src/views/Profile';
import More from './src/views/More';
import Prescription from './src/views/Prescription';
import AddPrescription from './src/views/AddPrescription';
import ViewPrescription from './src/views/ViewPrescription';
import Address from './src/views/Address';
import AddressList from './src/views/AddressList';
import Payment from './src/views/Payment';
import Login from './src/views/Login';
import Register from './src/views/Register';
import Faq from './src/views/Faq';
import FaqDetails from './src/views/FaqDetails';
import PrivacyPolicy from './src/views/PrivacyPolicy';
import Forgot from './src/views/Forgot';
import Otp from './src/views/Otp';
import Reset from './src/views/Reset';
import ContactUs from './src/views/ContactUs';
import Logout from './src/views/Logout';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#ffffff',
        inactiveTintColor: '#dfdfdf',
        style: {
          backgroundColor: colors.theme_fg,
          height: 60
        }
      }}

    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <Icon name='home' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="MyOrders"
        component={MyOrders}
        options={{
          tabBarLabel: 'MyOrders',
          tabBarIcon: ({ color, size }) => (
            <Icon name='file-text' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Cart"
        component={Cart}
        options={{
          tabBarLabel: 'Cart',
          tabBarIcon: ({ color, size }) => (
            <Icon name='shopping-bag' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({ color, size }) => (
            <Icon name='search' color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="More"
        component={More}
        options={{
          tabBarLabel: 'More',
          tabBarIcon: ({ color, size }) => (
            <Icon name='menu' color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}


function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="Splash" >
        <Stack.Screen name="AddPrescription" component={AddPrescription} />
        <Stack.Screen name="Address" component={Address} />
        <Stack.Screen name="AddressList" component={AddressList} />
        <Stack.Screen name="ContactUs" component={ContactUs} />
        <Stack.Screen name="Faq" component={Faq} />
        <Stack.Screen name="FaqDetails" component={FaqDetails} />
        <Stack.Screen name="Forgot" component={Forgot} />
        <Stack.Screen name="Home" component={MyTabs} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Logout" component={Logout} />
        <Stack.Screen name="OrderDetails" component={OrderDetails} />
        <Stack.Screen name="Otp" component={Otp} />
        <Stack.Screen name="Payment" component={Payment} />
        <Stack.Screen name="Prescription" component={Prescription} />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
        <Stack.Screen name="Product" component={Product} />
        <Stack.Screen name="ProductDetails" component={ProductDetails} />
        <Stack.Screen name="Promo" component={Promo} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Reset" component={Reset} />
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="SubCategory" component={SubCategory} />
        <Stack.Screen name="ViewPrescription" component={ViewPrescription} />
        <Stack.Screen name="Profile" component={Profile} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
